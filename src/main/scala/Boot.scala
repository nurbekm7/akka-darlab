import akka.actor.{ActorRef, ActorSystem, Props}
import akka.io.IO
import com.typesafe.config.ConfigFactory
import kz.darlab.l1.{ChildActor, MainActor}
import kz.darlab.l1.MainActor.Ping
import kz.darlab.l1.amqp.{AMQPConnectionConfig, AMQPConnectionCustom, AMQPConsumer, RequestPublisherActor}
import kz.darlab.l1.routing.{ListenerActor, RestRouting}
import spray.can.Http

import scala.util.Try

object Boot extends App {


  def getStringConfigKey(key: String): String = {
    try {
      sys.env(key)
    } catch {
      case e: NoSuchElementException =>
        config.getString(key)
    }
  }

  def getIntConfigKey(key: String): Int = {
    try {
      sys.env(key).toInt
    } catch {
      case e: NoSuchElementException =>
        config.getInt(key)
    }
  }

  implicit val system = ActorSystem("DARLab")

  val config = ConfigFactory.load()

  val host = config.getString("service.host")
  val port = config.getInt("service.port")

  val amqpConnection = new AMQPConnectionCustom(
    getStringConfigKey("amqp.connection.host"),
    getIntConfigKey("amqp.connection.port"),
    getStringConfigKey("amqp.connection.user"),
    getStringConfigKey("amqp.connection.password"),
    getIntConfigKey("amqp.connection.reconnectionDelay"),
    Try {
      getStringConfigKey("amqp.connection.virtualHost")
    }.toOption
  )

  val replyExchange = config.getString("amqp.endpoints.exchange")


  val publisherActor = system.actorOf(RequestPublisherActor.props(amqpConnection, replyExchange),name = "amqpPublisher")

  val responseListener = system.actorOf(ListenerActor.props(publisherActor), name = "amqpListener")

  val responseConsumer  = new AMQPConsumer(amqpConnection, responseListener, "reply")


//  val childActor = system.actorOf(ChildActor.props)
  val mainActor = system.actorOf(MainActor.props(publisherActor))
  val mainActorProps = (_: Unit) => MainActor.props(publisherActor)

  val routeActor = system.actorOf(Props(new RestRouting(mainActor, mainActorProps)))


  IO(Http) ! Http.Bind(routeActor, host, port = port)

}
