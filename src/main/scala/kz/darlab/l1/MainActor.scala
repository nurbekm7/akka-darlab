package kz.darlab.l1

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import kz.darlab.akka.domain.amqp.{DomainMessage, Msgs}
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, Person}
import kz.darlab.l1.ChildActor.Pong
import kz.darlab.l1.MainActor.{HandlePingRequest, Ping}
import kz.darlab.l1.amqp.RequestPublisherActor.PublishToQueue

object MainActor {
  case class Ping()

  case class HandlePingRequest(person: Person) extends ApiAkkaRequest

  def props(publisherActor: ActorRef):Props = Props(new MainActor(publisherActor))
}

class MainActor(publisherActor: ActorRef) extends Actor with ActorLogging {

  val actorName = self.path.toStringWithoutAddress

  def replyTo(apiMessage: String): String = {
    s"response.${apiMessage}"
  }

  def receive: Receive = {
    case HandlePingRequest(person) =>
      log.debug(s"Person is : $person")
      val request = DomainMessage.request[String](Msgs.Check, Some(replyTo("checkRequest")), Map(), "CHECK-MSG" )
      publisherActor ! PublishToQueue(request, Some(actorName))
      sender() ! person.copy(name = "person")
//      context.become(waitingResponse())


  }

//  def waitingResponse(): Receive = {
//
//    case resp : ResponseMsg =>
//      context.parent ! resp
//  }
}
