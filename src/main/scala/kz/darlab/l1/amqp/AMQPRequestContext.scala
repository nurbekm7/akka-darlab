package kz.darlab.l1.amqp


case class AMQPRequestContext(correlationId: String, replyTo: String, language: String)