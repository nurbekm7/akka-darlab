package kz.darlab.l1

import akka.actor.{Actor, ActorLogging, Props}
import kz.darlab.l1.ChildActor.Pong

object ChildActor {

  case class Pong()

  def props: Props = Props(new ChildActor)
}

class ChildActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case Pong =>
      log.info(s"Received Pong")
  }
}
