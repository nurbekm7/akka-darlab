package kz.darlab.l1.util

import kz.darlab.akka.domain.serializers.{CoreSerializer, DateTimeSerializer, PersonaSerializer}
import org.json4s.ShortTypeHints
import org.json4s.native.Serialization

trait SerializerWithTypeHint extends DateTimeSerializer with  PersonaSerializer with CoreSerializer   {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(ShortTypeHints(coreTypeHints ++
      personTypeHints))+
    new DateTimeSerializer()

}
