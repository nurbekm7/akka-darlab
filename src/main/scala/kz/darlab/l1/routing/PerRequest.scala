package kz.darlab.l1.routing

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, DomainAkka}
import kz.darlab.l1.routing.PerRequest.{WithActorRef, WithProps}
import kz.darlab.l1.util.SerializerWithTypeHint
import spray.http.StatusCode
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

trait PerRequest  extends  Actor with ActorLogging with Json4sSupport with SerializerWithTypeHint {

  val json4sFormats = formats

  def r: RequestContext
  def target: ActorRef
  def msg: ApiAkkaRequest

  target ! msg

  override def receive: Receive = {
    case res: DomainAkka => complete(StatusCode.int2StatusCode(200), res)
  }


  def complete[T<: AnyRef](value: StatusCode, akka: T): Unit = {
    r.complete(value, akka)
    context.stop(self)
  }

}

object PerRequest {
  case class WithActorRef(r: RequestContext, target: ActorRef, msg: ApiAkkaRequest) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, msg: ApiAkkaRequest, actorName: Option[String] = None) extends PerRequest {
    lazy val target = actorName match {
      case Some(name) => context.actorOf(props, name)
      case _ => context.actorOf(props)
    }
  }
}

trait PerRequestInit {
  this: Actor =>

  def perRequest(r: RequestContext, target: ActorRef, msg: ApiAkkaRequest) ={
    context.actorOf(Props(new WithActorRef(r, target,msg)))
  }
  def perRequest(r: RequestContext, props: Props, msg: ApiAkkaRequest) ={
    context.actorOf(Props(new WithProps(r, props,msg)))
  }

}


