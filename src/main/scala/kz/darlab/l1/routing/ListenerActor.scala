package kz.darlab.l1.routing

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.github.sstone.amqp.Amqp.{Ack, Delivery}
import kz.darlab.akka.domain.amqp.DomainMessage
import kz.darlab.l1.util.SerializerWithTypeHint
import org.json4s._
import org.json4s.native.JsonMethods._
import spray.httpx.Json4sSupport

object ListenerActor {
  def props(publisherActor: ActorRef): Props =  Props(classOf[ListenerActor], publisherActor)
}

class ListenerActor(publisherActor: ActorRef) extends Actor with SerializerWithTypeHint with Json4sSupport with ActorLogging {


  override implicit def json4sFormats : Formats = formats


  def parseCommand(msgBody: String): DomainMessage[_] = {
    parse(msgBody).extract[DomainMessage[_]]
  }

  override def receive = {
    case d @ Delivery(consumerTag, envelope, properties, body) => {
      val msgBody = new String(body,"utf-8")
      log.info("got message: {}",msgBody)

      sender ! Ack(envelope.getDeliveryTag)

      // парсим комманду
      val command = parseCommand(msgBody)


      command match {

        case resp: DomainMessage[_] => {
          val actorName = d.properties.getCorrelationId

          if(actorName != null) {

            val pingEchoActor = context.actorSelection(actorName)

            pingEchoActor ! command.body
          } else {
            log.error("Actor with provided ID was not found")
          }
        }
      }

    }
  }


}