package kz.darlab.l1.routing

import akka.actor.{Actor, ActorRef, Props}
import kz.darlab.akka.domain.entities.{ApiAkkaRequest, Person}
import kz.darlab.l1.MainActor.HandlePingRequest
import kz.darlab.l1.util.SerializerWithTypeHint
import spray.httpx.Json4sSupport
import spray.routing.{HttpService, Route}

import scala.concurrent.ExecutionContext


class RestRouting(mainActor: ActorRef,
                  mainActorProps: Unit => Props
                 ) extends HttpService  with Actor with Json4sSupport
      with SerializerWithTypeHint  with PerRequestInit {

  implicit val executionContext: ExecutionContext = context.dispatcher
  implicit def actorRefFactory = context

  override def receive: Receive = runRoute(route)
  implicit val json4sFormats = formats


  val route = pathPrefix("akka") {
    post {
      path("post") {
        entity(as[Person]) { person =>
          handleRequest(HandlePingRequest(person))
        }
      }
    }
  }

  def handleRequest(msg: ApiAkkaRequest): Route = {
    ctx => perRequest(ctx, mainActorProps(), msg)
  }
}
