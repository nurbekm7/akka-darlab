name := "Akka"

version := "0.1"

scalaVersion := "2.11.7"

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies ++=  Seq (
  "com.typesafe.akka" %% "akka-actor" % "2.5.6",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.6",
  "io.spray" %% "spray-can" % "1.3.3",
  "io.spray" %% "spray-routing" % "1.3.3",
  "org.json4s" %% "json4s-native" % "3.5.3",
  "com.rabbitmq"      %   "amqp-client"       % "3.5.3",
  "com.github.sstone" %   "amqp-client_2.11"  % "1.5",
  "joda-time" % "joda-time" % "2.8.2",
  "org.joda" % "joda-convert" % "1.7",
  "kz.darlab" %% "akka-domain" % "1.0.0"
)